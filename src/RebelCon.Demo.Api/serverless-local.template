{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Transform": "AWS::Serverless-2016-10-31",
    "Description": "An AWS Serverless Application that uses the ASP.NET Core framework running in Amazon Lambda.",
    "Parameters": {
        "environment": {
            "Type": "String",
            "Default": "development",
            "Description": "The environment to deploy to."
        }
    },
    "Resources": {
        "SeedFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Handler": "RebelCon.Demo.Api::RebelCon.Demo.Api.SeedFunction::SeedTable",
                "Runtime": "dotnetcore2.1",
                "CodeUri": "",
                "MemorySize": 1536,
                "Timeout": 60,
                "FunctionName": {
                    "Fn::Join": [
                        "-",
                        [
                            "RebelCon",
                            "Demo",
                            "Seed",
                            {
                                "Ref": "environment"
                            }
                        ]
                    ]
                },
                "Role": null,
                "Policies": [
                    "AWSLambdaFullAccess"
                ],
                "Environment": {
                    "Variables": {
                        "UNWRAP_AGGREGATE_EXCEPTIONS": true,
                        "ALGOLIA_APP_ID": "{{resolve:ssm:/rebelcon-demo/algolia/app-id:1}}",
                        "ALGOLIA_API_KEY": "{{resolve:ssm:/rebelcon-demo/algolia/api-key:1}}",
                        "ALGOLIA_API_INDEX": "{{resolve:ssm:/rebelcon-demo/algolia/api-index:1}}",
                        "ENV": {
                            "Ref": "environment"
                        },
                        "TABLE_NAME": {
                            "Ref": "Table"
                        },
                        "STREAM_NAME": {
                            "Fn::GetAtt": [
                                "Table",
                                "StreamArn"
                            ]
                        }
                    }
                }
            }
        },
        "SeedFunctionCustomResource": {
            "DependsOn": "Table",
            "Type": "Custom::TableSeedFunction",
            "Properties": {
                "ServiceToken": {
                    "Fn::GetAtt": [
                        "SeedFunction",
                        "Arn"
                    ]
                },
                "TableName": {
                    "Ref": "Table"
                }
            }
        },
        "TableStreamFunction": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Handler": "RebelCon.Demo.Api::RebelCon.Demo.Api.DataStreams::DataStreamsHandler",
                "Runtime": "dotnetcore2.1",
                "CodeUri": "",
                "MemorySize": 1024,
                "Timeout": 30,
                "FunctionName": {
                    "Fn::Join": [
                        "-",
                        [
                            "RebelCon",
                            "Demo",
                            "TableStream",
                            {
                                "Ref": "environment"
                            }
                        ]
                    ]
                },
                "Role": null,
                "Policies": [
                    "AWSLambdaFullAccess"
                ],
                "Events": {
                    "Stream": {
                        "Type": "DynamoDB",
                        "Properties": {
                            "Stream": {
                                "Fn::GetAtt": [
                                    "Table",
                                    "StreamArn"
                                ]
                            },
                            "StartingPosition": "TRIM_HORIZON",
                            "BatchSize": 500
                        }
                    }
                },
                "Environment": {
                    "Variables": {
                        "UNWRAP_AGGREGATE_EXCEPTIONS": true,
                        "ALGOLIA_APP_ID": "{{resolve:ssm:/rebelcon-demo/algolia/app-id:1}}",
                        "ALGOLIA_API_KEY": "{{resolve:ssm:/rebelcon-demo/algolia/api-key:1}}",
                        "ALGOLIA_API_INDEX": "{{resolve:ssm:/rebelcon-demo/algolia/api-index:1}}",
                        "ENV": {
                            "Ref": "environment"
                        }
                    }
                }
            }
        },
        "Table": {
            "Type": "AWS::DynamoDB::Table",
            "Properties": {
                "TableName": {
                    "Fn::Join": [
                        "-",
                        [
                            "RebelCon",
                            "Demo",
                            {
                                "Ref": "environment"
                            }
                        ]
                    ]
                },
                "AttributeDefinitions": [
                    {
                        "AttributeName": "Id",
                        "AttributeType": "S"
                    }
                ],
                "KeySchema": [
                    {
                        "AttributeName": "Id",
                        "KeyType": "HASH"
                    }
                ],
                "ProvisionedThroughput": {
                    "ReadCapacityUnits": "5",
                    "WriteCapacityUnits": "5"
                },
                "StreamSpecification": {
                    "StreamViewType": "NEW_IMAGE"
                }
            }
        },
        "Api": {
            "Type": "AWS::Serverless::Function",
            "Properties": {
                "Handler": "RebelCon.Demo.Api::RebelCon.Demo.Api.LambdaEntryPoint::FunctionHandlerAsync",
                "Runtime": "dotnetcore2.1",
                "FunctionName": {
                    "Fn::Join": [
                        "-",
                        [
                            "RebelCon",
                            "Demo",
                            "Api",
                            {
                                "Ref": "environment"
                            }
                        ]
                    ]
                },
                "CodeUri": "bin/Debug/netcoreapp2.1/publish",
                "MemorySize": 1024,
                "Timeout": 30,
                "Role": null,
                "Policies": [
                    "AWSLambdaFullAccess"
                ],
                "Environment": {
                    "Variables": {
                        "TABLE_NAME": {
                            "Ref": "Table"
                        },
                        "UNWRAP_AGGREGATE_EXCEPTIONS": true,
                        "ALGOLIA_APP_ID": "{{resolve:ssm:/rebelcon-demo/algolia/app-id:1}}",
                        "ALGOLIA_API_KEY": "{{resolve:ssm:/rebelcon-demo/algolia/api-key:1}}",
                        "ALGOLIA_API_INDEX": "{{resolve:ssm:/rebelcon-demo/algolia/api-index:1}}",
                        "ENV": {
                            "Ref": "environment"
                        }
                    }
                },
                "Events": {
                    "AnyRequestProxy": {
                        "Type": "Api",
                        "Properties": {
                            "Path": "/{proxy+}",
                            "Method": "ANY"
                        }
                    }
                }
            }
        }
    },
    "Outputs": {
        "ApiUrl": {
            "Description": "The URL for the API.",
            "Value": {
                "Fn::Sub": "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod"
            }
        }
    }
}
