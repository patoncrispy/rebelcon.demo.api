﻿using System;
using Algolia.Search.Clients;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Thundra.Agent.Log.AspNetCore;


namespace RebelCon.Demo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public static IConfiguration Configuration { get; private set; }

        private readonly IHostingEnvironment _env;

        public void ConfigureServices(IServiceCollection services)
        {
            if (_env.IsDevelopment())
            {
                services.Configure<AppSettings>(options => Configuration.Bind(options));
            }

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy =>
          {
                  policy.AllowAnyHeader();
                  policy.AllowAnyMethod();
                  policy.AllowAnyOrigin();
                  policy.AllowCredentials();
              });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<IAmazonDynamoDB>();

            services.AddSingleton<IDynamoDBContext, DynamoDBContext>((serviceFactory) =>
            {
                AmazonDynamoDBClient client = serviceFactory.GetRequiredService(typeof(IAmazonDynamoDB)) as AmazonDynamoDBClient;
                return new DynamoDBContext(client);
            });

            services.AddSingleton<ISearchClient, SearchClient>((serviceProvider) =>
            {
                IOptions<AppSettings> appSettings = serviceProvider.GetService<IOptions<AppSettings>>();
                var id = appSettings.Value.ALGOLIA_APP_ID;
                var key = appSettings.Value.ALGOLIA_API_KEY;
                return new SearchClient(id, key);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.UseHttpsRedirection();
                loggerFactory.AddThundraProvider();
            }

            app.UseCors();
            app.UseMvc();
        }
    }
}
