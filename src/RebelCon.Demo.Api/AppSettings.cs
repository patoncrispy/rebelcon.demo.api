﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RebelCon.Demo.Api
{
  public class AppSettings
  {
    public string ALGOLIA_API_INDEX { get; set; } = Environment.GetEnvironmentVariable(nameof(AppSettings.ALGOLIA_API_INDEX));
    public string ALGOLIA_API_KEY { get; set; } = Environment.GetEnvironmentVariable(nameof(AppSettings.ALGOLIA_API_KEY));
    public string ALGOLIA_APP_ID { get; set; } = Environment.GetEnvironmentVariable(nameof(AppSettings.ALGOLIA_APP_ID));
    public string TABLE_NAME { get; set; } = Environment.GetEnvironmentVariable(nameof(AppSettings.TABLE_NAME));
    public string ENV { get; set; } = Environment.GetEnvironmentVariable(nameof(AppSettings.ENV));
  }
}
