﻿aws --endpoint-url http://localhost:4572 s3api create-bucket --bucket rebelcon-demo-seed-bucket-2
aws --endpoint-url http://localhost:4572 s3api put-object --bucket rebelcon-demo-seed-bucket-2 --key winemag-data-1k.json
aws --endpoint-url http://localhost:4569 dynamodb create-table --table-name local --attribute-definitions AttributeName=Id,AttributeType=S --key-schema AttributeName=Id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5
