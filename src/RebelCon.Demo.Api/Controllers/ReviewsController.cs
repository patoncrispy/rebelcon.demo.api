
using System;
using System.Linq;
using System.Threading.Tasks;
using Algolia.Search.Clients;
using Algolia.Search.Models.Search;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace RebelCon.Demo.Api
{

    [ApiController]
    [Route("reviews")]
    public class ReviewsController : ControllerBase
    {
        private readonly ISearchClient _searchClient;
        private readonly ISearchIndex _searchIndex;
        private readonly IDynamoDBContext _db;
        private readonly string _tableName;

        public ReviewsController(IOptions<AppSettings> settings, ISearchClient searchClient, IDynamoDBContext db)
        {
            var index = settings.Value.ALGOLIA_API_INDEX;
            var env = settings.Value.ENV
            ;
            var indexName = $"{env}_{index}";

            _db = db;
            _searchClient = searchClient;
            _searchIndex = _searchClient.InitIndex(indexName);
            _tableName = settings.Value.TABLE_NAME;
        }

        [HttpGet("search/{page:int?}/{pageSize:int?}/{queryString?}")]
        public async Task<IActionResult> Search(int? page = 0, int? pageSize = 25, string queryString = "")
        {
            var query = new Query(queryString)
            {
                HitsPerPage = pageSize,
                Page = page
            };

            var result = await _searchIndex.SearchAsync<WineReviewModel>(query);

            if (result == null)
            {

                return Ok(new SearchResponse<WineReviewModel>
                {
                    Hits = new System.Collections.Generic.List<WineReviewModel>()
                });
            }

            return Ok(result);

        }

        [HttpDelete()]
        public async Task<IActionResult> Delete([FromBody] WineReviewModel model)
        {
            await _db.DeleteAsync(model, new DynamoDBOperationConfig { OverrideTableName = _tableName });
            return Ok();
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] WineReviewModel model)
        {
            await _db.SaveAsync(model, new DynamoDBOperationConfig { OverrideTableName = _tableName });
            return Ok();
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] WineReviewModel model)
        {
            await _db.SaveAsync(model, new DynamoDBOperationConfig { OverrideTableName = _tableName });
            return Ok();
        }
    }
}
