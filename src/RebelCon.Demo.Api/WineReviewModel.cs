using System;
using Newtonsoft.Json;

namespace RebelCon.Demo.Api
{
  public class WineReviewModel
  {
    [JsonProperty("objectID")]
    public string Id { get; set; } = Guid.NewGuid().ToString();
    public int? Points { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    [JsonProperty("taster_name")]
    public string TasterName { get; set; }
    [JsonProperty("taster_twitter_handle")]
    public string TasterTwitterHandle { get; set; }
    public int? Price { get; set; }
    public string Designation { get; set; }
    public string Variety { get; set; }
    [JsonProperty("region_1")]
    public string RegionOne { get; set; }
    [JsonProperty("region_2")]
    public string RegionTwo { get; set; }
    public string Province { get; set; }
    public string Country { get; set; }
    public string Winery { get; set; }
  }
}
