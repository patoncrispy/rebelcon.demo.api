using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Algolia.Search.Clients;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization.Json;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Thundra.Agent.Log.AspNetCore;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
namespace RebelCon.Demo.Api
{
    public class SeedFunction
    {
        private const string ALGOLIA_APP_ID = "ALGOLIA_APP_ID";
        private const string ALGOLIA_API_KEY = "ALGOLIA_API_KEY";
        private const string ALGOLIA_API_INDEX = "ALGOLIA_API_INDEX";
        private const string ENV = "ENV";
        private const string TABLE_NAME = "TABLE_NAME";
        private readonly ILogger<SeedFunction> _logger;
        private readonly IAmazonS3 s3;
        private readonly AmazonDynamoDBClient dbClient;
        private readonly IDynamoDBContext db;
        private readonly SearchClient _algoliaClient;
        private readonly string _indexName;
        private readonly SearchIndex _index;

        public SeedFunction()
        {
            var id = Environment.GetEnvironmentVariable(ALGOLIA_APP_ID);
            var key = Environment.GetEnvironmentVariable(ALGOLIA_API_KEY);
            var indexName = Environment.GetEnvironmentVariable(ALGOLIA_API_INDEX);
            var env = Environment.GetEnvironmentVariable(ENV);

            var loggerFactory = new LoggerFactory().AddThundraProvider();
            _logger = loggerFactory.CreateLogger<SeedFunction>();

            _algoliaClient = new SearchClient(id, key);
            _indexName = $"{env}_{indexName}";
            _index = _algoliaClient.InitIndex(_indexName);

            if (env == "local")
            {
                dbClient = new AmazonDynamoDBClient(new AmazonDynamoDBConfig
                {
                    ServiceURL = "http://localhost:4564"
                });

                s3 = new AmazonS3Client(new AmazonS3Config { ServiceURL = "http://localhost:4572" });
            }
            else
            {
                dbClient = new AmazonDynamoDBClient();
                s3 = new AmazonS3Client();
            }

            db = new DynamoDBContext(dbClient);
        }

        public async Task<CloudFormationResponse> SeedTable(CloudFormationRequest request, ILambdaContext context)
        {
            _logger.LogInformation($"Received Request: {JsonConvert.SerializeObject(request)}");
            var response = new CloudFormationResponse
            {
                StackId = request.StackId,
                LogicalResourceId = request.LogicalResourceId,
                RequestId = request.RequestId,
                NoEcho = false,
                Reason = "See CloudWatch logs for detail"
            };

            try
            {
                switch (request.RequestType.ToLower())
                {
                    case "create":
                        _logger.LogInformation("Getting data from S3.");
                        var data = await GetDataFromS3();
                        _logger.LogInformation("Received data.");
                        var table = Environment.GetEnvironmentVariable(TABLE_NAME);
                        _logger.LogInformation("Writing to table.");
                        Thread.Sleep(30 * 1000);
                        await LoadTableWithData(data);
                        _logger.LogInformation("Data sent to table successfully.");
                        break;

                    case "delete":
                        _logger.LogInformation("Deleting Algolia Index...");
                        await _index.DeleteAsync();
                        _logger.LogInformation("Algolia Index Deleted.");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, ex.Message);
                return await CloudFormationResponse.CompleteCloudFormationResponse(ex, request, context);
            }


            var cfResponse = await CloudFormationResponse.CompleteCloudFormationResponse(null, request, context);
            _logger.LogInformation($"Sending response: {JsonConvert.SerializeObject(response)}");
            return cfResponse;
        }

        private async Task<IEnumerable<WineReviewModel>> GetDataFromS3()
        {
            var getObjectRequest = new GetObjectRequest
            {
                BucketName = "rebelcon-demo-seed-bucket-2",
                Key = "winemag-data-1k.json",

            };

            var s3Response = await s3.GetObjectAsync(getObjectRequest);
            _logger.LogDebug($"Got data from S3: Status Code: {s3Response.HttpStatusCode}");
            if (s3Response.HttpStatusCode == HttpStatusCode.OK)
            {
                _logger.LogInformation($"Got S3 Object...");
                var reader = new StreamReader(s3Response.ResponseStream);
                var jsonReader = new JsonTextReader(reader);
                var serializer = new Newtonsoft.Json.JsonSerializer();
                var result = serializer.Deserialize<List<WineReviewModel>>(jsonReader);
                _logger.LogInformation($"Read stream complete. Items: {result.Count}");

                return result;
            }

            return new List<WineReviewModel>();
        }

        private async Task LoadTableWithData(IEnumerable<WineReviewModel> reviews)
        {
            var table = Environment.GetEnvironmentVariable(TABLE_NAME);
            var batchWrite = db.CreateBatchWrite<WineReviewModel>(new DynamoDBOperationConfig
            {
                OverrideTableName = table
            });
            batchWrite.AddPutItems(reviews);
            await batchWrite.ExecuteAsync();
        }
    }
}
