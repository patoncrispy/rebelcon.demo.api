using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Thundra.Agent.Log.AspNetCore;
using Microsoft.Extensions.Configuration;

namespace RebelCon.Demo.Api
{
  /// <summary>
  /// This class extends from APIGatewayProxyFunction which contains the method FunctionHandlerAsync which is the 
  /// actual Lambda function entry point. The Lambda handler field should be set to
  /// 
  /// RebelCon.Demo.Api::RebelCon.Demo.Api.LambdaEntryPoint::FunctionHandlerAsync
  /// </summary>
  public class LambdaEntryPoint : Amazon.Lambda.AspNetCoreServer.APIGatewayProxyFunction
  {
    /// <summary>
    /// The builder has configuration, logging and Amazon API Gateway already configured. The startup class
    /// needs to be configured in this method using the UseStartup<>() method.
    /// </summary>
    /// <param name="builder"></param>
    protected override void Init(IWebHostBuilder builder)
    {
            var config = new ConfigurationBuilder().AddEnvironmentVariables().Build();

      builder
                .UseConfiguration(config)
      .UseStartup<Startup>();
    }
  }
}
