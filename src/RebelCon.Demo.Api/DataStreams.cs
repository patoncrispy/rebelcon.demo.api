using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Algolia.Search.Clients;
using Algolia.Search.Models.Settings;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Thundra.Agent.Log.AspNetCore;

namespace RebelCon.Demo.Api
{
  public class DataStreams
  {
    private const string ALGOLIA_APP_ID = "ALGOLIA_APP_ID";
    private const string ALGOLIA_API_KEY = "ALGOLIA_API_KEY";
    private const string ALGOLIA_API_INDEX = "ALGOLIA_API_INDEX";
    private const string ENV = "ENV";
    private readonly ILogger<DataStreams> _logger;
    private readonly ISearchClient _algoliaClient;
    private readonly string _indexName;
    private readonly ISearchIndex _index;

    public DataStreams()
    {
      var id = Environment.GetEnvironmentVariable(ALGOLIA_APP_ID);
      var key = Environment.GetEnvironmentVariable(ALGOLIA_API_KEY);
      var indexName = Environment.GetEnvironmentVariable(ALGOLIA_API_INDEX);
      var env = Environment.GetEnvironmentVariable(ENV);

      var loggerFactory = new LoggerFactory().AddThundraProvider();
      _logger = loggerFactory.CreateLogger<DataStreams>();

      _algoliaClient = new SearchClient(id, key);
      _indexName = $"{env}_{indexName}";
      _index = _algoliaClient.InitIndex(_indexName);

      var settings = new IndexSettings
      {
        SearchableAttributes = new List<string> {
            GetPropertyJsonKey(nameof(WineReviewModel.Title)),
            GetPropertyJsonKey(nameof(WineReviewModel.Description)),
            GetPropertyJsonKey(nameof(WineReviewModel.Country)),
            GetPropertyJsonKey(nameof(WineReviewModel.Designation)),
            GetPropertyJsonKey(nameof(WineReviewModel.Province)),
            GetPropertyJsonKey(nameof(WineReviewModel.RegionOne)),
            GetPropertyJsonKey(nameof(WineReviewModel.RegionTwo))
          }
      };

      _index.SetSettings(settings);
    }

    public async Task DataStreamsHandler(DynamoDBEvent payload, ILambdaContext context)
    {
      _logger.LogInformation($"Processing stream event.");
      _logger.LogInformation($"{payload.Records.Count} records received from db stream.");

      var upsertRecords = new List<Record>();
      var deleteRecords = new List<Record>();

      foreach (var record in payload.Records)
      {
        if (record.EventName == OperationType.INSERT || record.EventName == OperationType.MODIFY)
        {
          upsertRecords.Add(record);
        }
        else if (record.EventName == OperationType.REMOVE)
        {
          deleteRecords.Add(record);
        }
      }

      try
      {
        await UpsertRecords(upsertRecords);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, $"An error occurred upserting records: {ex.Message}");
      }

      try
      {
        await DeleteRecords(deleteRecords);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, $"An error occurred deleting records: {ex.Message}");
      }
    }

    private async Task DeleteRecords(IList<Record> records)
    {
      if (!records.Any())
      {
        return;
      }

      _logger.LogInformation($"Deleting {records.Count} records.");
      _logger.LogDebug(JsonConvert.SerializeObject((records)));

      var modelIds = records
        .Select(record => record.Dynamodb.Keys["Id"]?.S)
        .Where(id => !string.IsNullOrEmpty(id))
        .ToList();

      await _index.DeleteObjectsAsync(modelIds);

      _logger.LogInformation("Items successfully deleted.");
    }

    private async Task UpsertRecords(IList<Record> records)
    {
      if (!records.Any())
      {
        return;
      }

      _logger.LogInformation($"Upserting {records.Count} records.");
      _logger.LogDebug(JsonConvert.SerializeObject((records)));


      var models = records.Select(record => MapImageToModel(record.Dynamodb.NewImage));
      await _index.SaveObjectsAsync(models);
      _logger.LogInformation("Items successfully upserted..");
    }



    private static WineReviewModel MapImageToModel(Dictionary<string, AttributeValue> image)
    {
      var model = new WineReviewModel();

      int.TryParse(image.ContainsKey("Points") ? image["Points"]?.N : string.Empty, out int points);
      int.TryParse(image.ContainsKey("Price") ? image["Price"]?.N : string.Empty, out int price);

      model.Id = image["Id"]?.S;
      model.Points = points;
      model.Title = image.ContainsKey("Title") ? image["Title"]?.S : string.Empty;
      model.Description = image.ContainsKey("Description") ? image["Description"]?.S : string.Empty;
      model.TasterName = image.ContainsKey("TasterName") ? image["TasterName"]?.S : string.Empty;
      model.TasterTwitterHandle = image.ContainsKey("TasterTwitterHandle") ? image["TasterTwitterHandle"]?.S : string.Empty;
      model.Price = price;
      model.Designation = image.ContainsKey("Designation") ? image["Designation"]?.S : string.Empty;
      model.Variety = image.ContainsKey("Variety") ? image["Variety"]?.S : string.Empty;
      model.RegionOne = image.ContainsKey("RegionOne") ? image["RegionOne"]?.S : string.Empty;
      model.RegionTwo = image.ContainsKey("RegionTwo") ? image["RegionTwo"]?.S : string.Empty;
      model.Province = image.ContainsKey("Province") ? image["Province"]?.S : string.Empty;
      model.Country = image.ContainsKey("Country") ? image["Country"]?.S : string.Empty;
      model.Winery = image.ContainsKey("Winery") ? image["Winery"]?.S : string.Empty;
      return model;
    }

    private string GetPropertyJsonKey(string propName)
    {
      return typeof(WineReviewModel)
      ?.GetProperty(propName)
      ?.GetCustomAttribute<JsonPropertyAttribute>()
      ?.PropertyName ?? $"{Char.ToLowerInvariant(propName[0])}{propName.Substring(1)}";
    }
  }
}

