#!/usr/bin/env bash

export AWS_DEFAULT_REGION=eu-west-2
STACK_NAME="RebelCon-Demo-Api-$1"
aws cloudformation delete-stack --stack-name ${STACK_NAME}