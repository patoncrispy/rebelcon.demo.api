#!/usr/bin/env bash
ENV=$1
BUCKET="rebelcon-demo-api-$ENV"
set -e

createBucketIfMissing() {
    local S3_BUCKET=$1;
    local REGION=$2;
    
    echo "Bucket: $S3_BUCKET"
    echo "Region: $REGION"
    
    echo "Checking S3 bucket $S3_BUCKET exists..."
    BUCKET_EXISTS=false
    set +e
    S3_CHECK=$(aws s3 ls "s3://${S3_BUCKET}" 2>&1)
    aws s3 ls "s3://${S3_BUCKET}"
    S3_CHECK_SUCCESS=$?
    set -e
    #Some sort of error happened with s3 check
    if [[ $S3_CHECK_SUCCESS != 0 ]]
    then
        NO_BUCKET_CHECK=$(echo ${S3_CHECK} | grep -c 'NoSuchBucket')
        if [[ ${NO_BUCKET_CHECK} = 1 ]]; then
            echo "Bucket does not exist"
            BUCKET_EXISTS=false
            aws s3 mb "s3://$S3_BUCKET" --region ${REGION}
        else
            echo "Error checking S3 Bucket"
            echo "$S3_CHECK"
            exit 1
        fi
    else
        echo "Bucket exists"
    fi
}

createBucketIfMissing $BUCKET $AWS_DEFAULT_REGION

set +e
dotnet tool install -g Amazon.Lambda.Tools
set -e
export PATH="$PATH:/root/.dotnet/tools"
export DOTNET_ROOT="$(dirname $(which dotnet))"

cd src/RebelCon.Demo.Api

dotnet lambda deploy-serverless \
-c Release \
-tp environment=$ENV \
--disable-interactive true \
--stack-name "RebelCon-Demo-Api-$ENV" \
--s3-bucket $BUCKET \
--region ${AWS_DEFAULT_REGION} \
--aws-access-key-id ${AWS_ACCESS_KEY_ID} \
--aws-secret-key ${AWS_SECRET_ACCESS_KEY}